import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

// 下面的情况，默认会导入@/views/login下的index.vue组件
import Login from '@/views/login'

Vue.use(Router)

// 配置路由
export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/login',
      name: 'login', // 路由名称
      component: Login // 组件对象
    }
  ]
})
